﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private float enemyWidth;
    [SerializeField] private float enemyHeight;
    [SerializeField] private float addPositionX;
    [SerializeField] private float addPositionY;
    [SerializeField] private int endScore;
    
    public void SpawnEnemy()
    {
        

        for (float y = enemyHeight; y <= 4; y += addPositionY)
        {
            for (float x = enemyWidth; x <= 7; x += addPositionX)
            {
                //SpawnEnemySpaceship(x, y);
            }
        }
            
        /*SpawnEnemySpaceship(5.30f, 3.17f);
        SpawnEnemySpaceship(-5.30f, 3.17f);
        SpawnEnemySpaceship(0, 3.17f);*/
    }
}
